package ga.jakeobrien.electondata;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import ga.jakeobrien.electondata.utils.Candidate;
import ga.jakeobrien.electondata.utils.County;
import ga.jakeobrien.electondata.utils.Precinct;

import java.io.*;
import java.net.URL;

public class JSONParser {
    public void parseCandidates() {
        Gson gson = new Gson();
        try {
            JsonArray json = gson.fromJson(new BufferedReader(new FileReader(getFileFromResources("candidates.json"))), JsonArray.class);
            json.forEach(e -> {
                new Candidate(e.getAsJsonObject().get("name").getAsString(),
                        e.getAsJsonObject().get("party").getAsString());
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void parsePrecincts() {
        Gson gson = new Gson();
        try {
            JsonArray json = gson.fromJson(new BufferedReader(new FileReader(getFileFromResources("data.json"))), JsonArray.class);
            json.forEach(e -> {
                String countyName = e.getAsJsonObject().get("County/City").getAsString();
                if (!County.countyExists(countyName)) {
                    new County(countyName);
                }
                Precinct p = new Precinct(e.getAsJsonObject().get("Pct").getAsString());

                for (Candidate c : Candidate.candidates) {
                   p.addCandidate(c, stringToInt(e.getAsJsonObject().get(c.getName()).getAsString()));
                }
                County.getCounty(countyName).addPrecinct(p);
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private int stringToInt(String s) {
        return Integer.parseInt(s.replaceAll(",",""));
    }

    private File getFileFromResources(String fileName) throws FileNotFoundException {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new FileNotFoundException();
        } else {
            return new File(resource.getFile());
        }

    }

    private static void printFile(File file) throws IOException {
        if (file == null) return;
        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        }
    }


}
