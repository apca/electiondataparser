package ga.jakeobrien.electondata;

import ga.jakeobrien.electondata.statistics.Party;
import ga.jakeobrien.electondata.statistics.Place;
import ga.jakeobrien.electondata.statistics.RawVotes;
import ga.jakeobrien.electondata.statistics.VotePercentage;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JSONParser parser = new JSONParser();
        parser.parseCandidates();
        parser.parsePrecincts();
        initGUI();
    }


    private static void initGUI() {
        JFrame frame = new JFrame("GUI");
        MainGUI form = new MainGUI();

        form.addStatistic(new Party());
        form.addStatistic(new RawVotes());
        form.addStatistic(new VotePercentage());
        form.addStatistic(new Place());

        form.populateCounties();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(960,720);
        frame.setContentPane(form.getMainPanel());
        frame.setVisible(true);
    }
}
