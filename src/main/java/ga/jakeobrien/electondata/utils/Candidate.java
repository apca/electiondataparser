package ga.jakeobrien.electondata.utils;

import java.util.ArrayList;

public class Candidate {
    public static ArrayList<Candidate> candidates = new ArrayList<>();
    private String name;
    private String party;

    public Candidate(String name, String party) {
        this.name = name;
        this.party = party;

        candidates.add(this);
    }

    public String getName() {
        return name;
    }

    public String getParty() {
        return party;
    }
}
