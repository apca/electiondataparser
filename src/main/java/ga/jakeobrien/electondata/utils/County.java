package ga.jakeobrien.electondata.utils;

import java.util.ArrayList;

public class County {
    public static ArrayList<County> counties = new ArrayList<>();
    private String name;
    private ArrayList<Precinct> precincts = new ArrayList<>();

    public County(String name) {
        this.name = name;
        counties.add(this);
    }

    public static boolean countyExists(String s) {
        for (County c : counties) {
            if (c.getName().equals(s)) {
                return true;
            }
        }
        return false;
    }

    public static County getCounty(String name) {
        for (County c : counties) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public void addPrecinct(Precinct p) {
        precincts.add(p);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Precinct> getPrecincts() {
        return precincts;
    }

    public Precinct getPrecinctByName(String n) {
        for (Precinct p : precincts) {
            if (p.getName().equals(n)) {
                return p;
            }
        }
        return null;
    }
}
