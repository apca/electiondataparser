package ga.jakeobrien.electondata.utils;

import java.util.HashMap;

public class Precinct {
    private String name;
    private HashMap<Candidate, Integer> votes = new HashMap<Candidate, Integer>();

    public Precinct(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addCandidate(Candidate c, int votes) {
        this.votes.put(c, votes);
    }

    public int getVotesForCandidate(Candidate c) {
        return votes.get(c);
    }
}
