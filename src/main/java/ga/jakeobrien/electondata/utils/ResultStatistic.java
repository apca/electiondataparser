package ga.jakeobrien.electondata.utils;

public interface ResultStatistic {
    String getName();
    Object getOutput(Candidate c, Precinct p);
}
