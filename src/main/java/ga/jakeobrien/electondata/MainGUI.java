package ga.jakeobrien.electondata;

import ga.jakeobrien.electondata.utils.Candidate;
import ga.jakeobrien.electondata.utils.County;
import ga.jakeobrien.electondata.utils.ResultStatistic;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

public class MainGUI {
    private JComboBox countyComboBox;
    private JComboBox precinctComboBox;
    private JButton showResultsButton;
    private JTable resultsTable;
    private JPanel mainPanel;

    private ArrayList<ResultStatistic> statistics = new ArrayList<>();

    public MainGUI() {
        countyComboBox.addActionListener(e -> populatePrecincts());
        showResultsButton.addActionListener(e -> populateResultsTable());
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void populateCounties() {
        for (County c : County.counties) {
            countyComboBox.addItem(c.getName());
        }
    }

    public void addStatistic(ResultStatistic s) {
        statistics.add(s);
    }

    private void populatePrecincts() {
        precinctComboBox.removeAllItems();
        County.getCounty((String) countyComboBox.getSelectedItem()).getPrecincts().forEach(p ->
        {
            precinctComboBox.addItem(p.getName());
        });
    }

    private void populateResultsTable() {
        DefaultTableModel dtm = new DefaultTableModel();
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Statistic");
        Candidate.candidates.forEach(c -> headers.add(c.getName()));
        dtm.setColumnIdentifiers(headers.toArray(new String[0]));
        dtm.setColumnCount(headers.size());

        resultsTable.setModel(dtm);

        for (ResultStatistic s : statistics) {
            ArrayList<Object> row = new ArrayList<>();
            row.add(s.getName());
            for (Candidate c : Candidate.candidates) {
                row.add(s.getOutput(c, County.getCounty(countyComboBox.getSelectedItem().toString())
                        .getPrecinctByName(precinctComboBox.getSelectedItem().toString())));
            }
            dtm.addRow(row.toArray());
        }
    }
}
