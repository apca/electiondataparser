package ga.jakeobrien.electondata.statistics;

import ga.jakeobrien.electondata.utils.Candidate;
import ga.jakeobrien.electondata.utils.Precinct;
import ga.jakeobrien.electondata.utils.ResultStatistic;

public class RawVotes implements ResultStatistic {
    @Override
    public String getName() {
        return "Raw Vote Counts";
    }

    @Override
    public Object getOutput(Candidate c, Precinct p) {
        return p.getVotesForCandidate(c);
    }
}
