package ga.jakeobrien.electondata.statistics;

import ga.jakeobrien.electondata.utils.Candidate;
import ga.jakeobrien.electondata.utils.Precinct;
import ga.jakeobrien.electondata.utils.ResultStatistic;

public class VotePercentage implements ResultStatistic {

    @Override
    public String getName() {
        return "Vote Percentage";
    }

    @Override
    public Object getOutput(Candidate c, Precinct p) {
        int total = 0;
        for (Candidate c2 : Candidate.candidates) {
            total += p.getVotesForCandidate(c2);
        }

        return Math.round(p.getVotesForCandidate(c)*10000.0/total)/100.0 + "%";
    }
}
