package ga.jakeobrien.electondata.statistics;

import ga.jakeobrien.electondata.utils.Candidate;
import ga.jakeobrien.electondata.utils.Precinct;
import ga.jakeobrien.electondata.utils.ResultStatistic;

public class Party implements ResultStatistic {
    @Override
    public String getName() {
        return "Party";
    }

    @Override
    public Object getOutput(Candidate c, Precinct p) {
        return c.getParty();
    }
}
