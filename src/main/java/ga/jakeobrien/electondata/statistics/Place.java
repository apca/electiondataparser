package ga.jakeobrien.electondata.statistics;

import ga.jakeobrien.electondata.utils.Candidate;
import ga.jakeobrien.electondata.utils.Precinct;
import ga.jakeobrien.electondata.utils.ResultStatistic;

import java.util.ArrayList;
import java.util.Collections;

public class Place implements ResultStatistic {
    @Override
    public String getName() {
        return "Place";
    }

    @Override
    public Object getOutput(Candidate c, Precinct p) {
        ArrayList<Integer> scores = new ArrayList<>();
        for (Candidate c2 : Candidate.candidates) {
            scores.add(p.getVotesForCandidate(c2));
        }
        Collections.sort(scores);
        Collections.reverse(scores);
        return scores.indexOf(p.getVotesForCandidate(c)) + 1;
    }
}
